"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./lib/main");
const child_proc = require("child_process");
exports.SERVER_VERSION = 3;
console.log("Running server version " + exports.SERVER_VERSION);
console.log("Last commit: " + child_proc.execSync('git show --summary --format="%ar: \\"%s\\""').toString("utf8").replace("\n", ""));
exports.DIR = __dirname + "";
const main = new main_1.Main();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHFDQUFrQztBQUNsQyw0Q0FBMkM7QUFFOUIsUUFBQSxjQUFjLEdBQUcsQ0FBQyxDQUFDO0FBRWhDLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEdBQUcsc0JBQWMsQ0FBQyxDQUFBO0FBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsNkNBQTZDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO0FBRXZILFFBQUEsR0FBRyxHQUFHLFNBQVMsR0FBRyxFQUFFLENBQUE7QUFDakMsTUFBTSxJQUFJLEdBQUcsSUFBSSxXQUFJLEVBQUUsQ0FBQyJ9