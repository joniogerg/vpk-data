import { DataResolver } from "./DataResolver";
import { TaskManager } from "./TaskManager";
import { RESTView } from "./RESTView";
import { Auth, AuthMethod } from "./Auth";
import { NotificationManager } from "./NotificationManager";

export class Main{
    

    private dataResolver = new DataResolver();
    private notificationManager = new NotificationManager();
    private taskManager = new TaskManager(this.dataResolver, this.notificationManager);
    private auth = new Auth();

    private restView = new RESTView(this.auth, this.dataResolver);

    constructor(){

        this.taskManager.startInterval();


    }

}