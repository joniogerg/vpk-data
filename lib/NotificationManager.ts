const https = require("https")
const config = require("./../config")

export class NotificationManager{




    checkForUpdates(){

    }

    sendNotification(){
        var key = config.notificationManager.key;
        var req = https.request({
            host: "fcm.googleapis.com",
            path: "/fcm/send",
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "key=" + key
            }
        }, res => {
            res.setEncoding("utf8")
            res.on("data", chunk => {
                console.log(chunk)
            })
        })

        
        req.write(JSON.stringify({
            data: {

            },
            condition: "!('t' in topics)"
        }))
        req.end();
    }
}