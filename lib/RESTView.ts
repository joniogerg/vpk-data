import * as express from "express"
import { Auth, AuthMethod } from "./Auth";
import { DataResolver } from "./DataResolver";

export class RESTView{
    
    private app = express();

    constructor(public auth: Auth, public data: DataResolver){

        this.app.use((req, res, next) => {
            req["authenticator"] = this.auth; 
            next();
        })

        this.app.get("/", this.authenticate, (req, res) => {
            // auth.authenticate(AuthMethod.STATIC, "karo", "ka-gy-ro-19").then(response => {
            //     console.log(response)
            //     res.send(response)
            // })

            res.send({
                successful: true
            })
            
        })
        
        this.app.get("/auth", (req, res) => {
            if(!req.query.username || !req.query.password){
                res.send({
                    error: "Kein Benutzername oder Password angegeben"
                })
                return;
            }
            auth.authenticate(AuthMethod.STATIC, req.query.username, req.query.password).then(response => {
                res.send(response);
            }).catch(() => {
                res.send({
                    error: "Benutzername oder Password sind falsch"
                })
            })
        })


        this.app.get("/filtered/:course", this.authenticate, (req, res) => {
            var filtered = this.data.getReplacementsFiltered(req.params["course"], req.query.teachers || "");
            
            filtered.days = filtered.days.slice(0, parseInt(req.query.days != undefined ? req.query.days : 2))
            res.send(filtered)
        })

        this.app.get("/unfiltered", this.authenticate, (req, res) => {
            res.send(this.data.getReplacements())
        })


        this.app.listen(8085)

    }


    authenticate(req, res, next){
        var rejectRequest = function(){
            res.status(401)
            res.send({
                error: "Unauthorized",
                errorCode: 401
            })
        }
        if(!req.headers["x-authentication"]){
            rejectRequest()
            return;
        }else{
            req.authenticator.authenticate(AuthMethod.TOKEN, req.headers["x-authentication"] as string).then(authRes => {
                if(authRes.valid){
                    next();
                }else{
                    rejectRequest();
                }
            })
        }
    }


}