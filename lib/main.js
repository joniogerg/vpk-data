"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DataResolver_1 = require("./DataResolver");
const TaskManager_1 = require("./TaskManager");
const RESTView_1 = require("./RESTView");
const Auth_1 = require("./Auth");
const NotificationManager_1 = require("./NotificationManager");
class Main {
    constructor() {
        this.dataResolver = new DataResolver_1.DataResolver();
        this.notificationManager = new NotificationManager_1.NotificationManager();
        this.taskManager = new TaskManager_1.TaskManager(this.dataResolver, this.notificationManager);
        this.auth = new Auth_1.Auth();
        this.restView = new RESTView_1.RESTView(this.auth, this.dataResolver);
        this.taskManager.startInterval();
    }
}
exports.Main = Main;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpREFBOEM7QUFDOUMsK0NBQTRDO0FBQzVDLHlDQUFzQztBQUN0QyxpQ0FBMEM7QUFDMUMsK0RBQTREO0FBRTVELE1BQWEsSUFBSTtJQVViO1FBUFEsaUJBQVksR0FBRyxJQUFJLDJCQUFZLEVBQUUsQ0FBQztRQUNsQyx3QkFBbUIsR0FBRyxJQUFJLHlDQUFtQixFQUFFLENBQUM7UUFDaEQsZ0JBQVcsR0FBRyxJQUFJLHlCQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMzRSxTQUFJLEdBQUcsSUFBSSxXQUFJLEVBQUUsQ0FBQztRQUVsQixhQUFRLEdBQUcsSUFBSSxtQkFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBSTFELElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7SUFHckMsQ0FBQztDQUVKO0FBakJELG9CQWlCQyJ9