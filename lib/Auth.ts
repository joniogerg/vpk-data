import config = require("./../config")
import * as jwt from "jsonwebtoken"
import { Database, OPEN_CREATE, OPEN_READWRITE, verbose as sqlite3_verbose } from "sqlite3"
import { DIR } from "./../index"

export class Auth{

    private SECRET = config.auth.secret;
    private DEFAULT_LIFETIME =              1000 * 60 * 60 * 24 * 30 * 11 // Lifetime 11 months
    private LAST_VALIDATION_THREADHOLD =    1000 * 60 * 60 * 24 * 7 * 2  // Threadhold 2 week

    private db: Database;

    constructor(){
        sqlite3_verbose();
        console.log("Opening database at " + DIR + "/db/auth.db")
        this.db = new Database(DIR + "/db/auth.db", OPEN_READWRITE | OPEN_CREATE, (err) => {
            if(err){
                console.error(err.message)
            }else{
                console.log("Connected to database ", process.cwd())
                this.db.run('CREATE TABLE IF NOT EXISTS tokens(token TEXT, lastValidation FLOAT)');
                this.cleanDatabase();
            }
        })
    }

    public authenticate(method: AuthMethod, username: string, password?: string): Promise<AuthResponse>{
        return new Promise<AuthResponse>((resolve, reject) => {
            if(method == AuthMethod.STATIC){
                if(username == config.auth.static.username && password == config.auth.static.password){
                    this.createToken(this.DEFAULT_LIFETIME).then( token => {
                        resolve({
                            method: method,
                            token: token
                        })
                    }).catch(err => {
                        console.error(err)
                    });
                }else{
                    reject(false)
                }
            }else if(method == AuthMethod.TOKEN){
                this.validateToken(username).then(valid => {
                    resolve({
                        method: method,
                        valid: valid
                    })
                })
            }
        })
    }

    private validateToken(token): Promise<boolean>{
        return new Promise((resolve, reject) => {
            this.db.get("SELECT * FROM tokens WHERE token=?", [token], (err, row) => {
                if(err == null && row != null && row.lastValidation + this.LAST_VALIDATION_THREADHOLD > new Date().getTime()){
                    this.db.run("UPDATE 'tokens' SET lastValidation=? WHERE token=?", [new Date().getTime(), token], (err) => {
                        resolve(true)
                    })
                }else{
                    this.db.run("DELETE FROM tokens WHERE token=?", [token], this.silenterr)
                    resolve(false)
                }
            }) 
        })
    }

    private createToken(lifetime: number): Promise<string>{
        return new Promise((resolve, reject) => {
            var token = jwt.sign({
                expiresAfter: new Date().getTime() + lifetime
            }, this.SECRET)
            this.db.run("INSERT INTO tokens VALUES(?, ?)", [token, new Date().getTime()], (err) => {
                console.log(err);
                if(err){
                    reject(err);
                }else{
                    resolve(token);
                }
            })
        })
    }

    private cleanDatabase(){
        this.db.run("DELETE FROM tokens WHERE lastValidation < ?", [new Date().getTime() - this.LAST_VALIDATION_THREADHOLD], this.silenterr)
    }

    private silenterr(err){
        if(err)
            console.error(err);
    }
    
}

export enum AuthMethod{
    STATIC = 0,
    TOKEN = 1,
}

export interface AuthResponse{
    method: AuthMethod,
    token?: string,
    err?: Error | string,
    valid?: boolean
}