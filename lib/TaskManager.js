"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TaskManager {
    constructor(dataResolver, notify) {
        this.dataResolver = dataResolver;
        this.notify = notify;
        this.DATA_REFRESH = 1000 * 20;
        this.intervalRunning = null;
    }
    startInterval() {
        this.intervalRunning = true;
        this.resolveData();
    }
    stopInterval() {
        this.intervalRunning = false;
    }
    resolveData() {
        var self = this;
        function doAgainLater() {
            setTimeout(() => {
                if (self.intervalRunning) {
                    self.resolveData();
                }
            }, self.DATA_REFRESH);
        }
        this.dataResolver.resolveData().then(data => {
            if (data.doNotify) {
            }
            doAgainLater();
        }).catch(err => {
            console.error(err);
            doAgainLater();
        });
    }
}
exports.TaskManager = TaskManager;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVGFza01hbmFnZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJUYXNrTWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLE1BQWEsV0FBVztJQU1wQixZQUFvQixZQUEwQixFQUFVLE1BQTJCO1FBQS9ELGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBcUI7UUFKbkYsaUJBQVksR0FBRyxJQUFJLEdBQUMsRUFBRSxDQUFDO1FBRWYsb0JBQWUsR0FBRyxJQUFJLENBQUM7SUFJL0IsQ0FBQztJQUdELGFBQWE7UUFDVCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixTQUFTLFlBQVk7WUFDakIsVUFBVSxDQUFFLEdBQUcsRUFBRTtnQkFDYixJQUFHLElBQUksQ0FBQyxlQUFlLEVBQUM7b0JBQ3BCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDdEI7WUFDTCxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBQ3pCLENBQUM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUMsRUFBRTtZQUV6QyxJQUFHLElBQUksQ0FBQyxRQUFRLEVBQUM7YUFFaEI7WUFHRCxZQUFZLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDWCxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLFlBQVksRUFBRSxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztDQUlKO0FBN0NELGtDQTZDQyJ9