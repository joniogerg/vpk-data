import * as https from "https"
import * as iconv from "iconv-lite"
import * as jsdom from "jsdom"     
import * as moment from "moment"
import * as fs from "fs";
const { JSDOM } = jsdom;           
import config = require("./../config")

export class DataResolver{

    private data: ReplacementData = {  lastCrawl: 1022130, lastUpdate: 1023123, days: []};


    getReplacements(): ReplacementData{
        return this.data
    }

    getReplacementsFiltered(course: string, teachersS: string = ""): ReplacementData{
        if(this.data == null){
            return null;
        }

        var data: ReplacementData = {
            days: [],
            lastUpdate: this.data.lastUpdate,
            lastCrawl: this.data.lastCrawl
        };

        for(let d of this.data.days){
            data.days.push({
                date: d.date,
                replacements: d.replacements,
                news: d.news
            })
        }
        var grade = this.parseGrade(course)
        var teachers = teachersS.split(" ");

        var isInSeniorLevel = grade.grade >= 11;
        
        for(let i = 0; i < data.days.length; i++){
            var day = data.days[i];
            var replacements = [];
            for(let repl of day.replacements){
                if(isInSeniorLevel && (repl.grade.grade == grade.grade && repl.grade.addons.includes("Q"))){
                    if(teachers.includes(repl.away)){
                        replacements.push(repl);
                        continue;
                    }
                }
                if(repl.grade.grade == grade.grade){
                    if(grade.addons.length == 0){
                        replacements.push(repl);
                    }else{
                        for(let a of grade.addons){
                            if(repl.grade.addons.includes(a)){
                                replacements.push(repl);
                                continue;
                            }
                        }
                    }
                }
            }
            data.days[i].replacements = replacements;
        }

        return data;
    }

    private parseGrade(s: string): Grade{
        if(typeof s === "object"){
            return s;
        }
        if(s == "Q11" || s == "Q12"){
            return {
                grade: parseInt(s.replace("Q", "")),
                addons: ["Q"]
            }
        }

        var isNum = false;
        var numDone = false;
        var addons = []
        var grade = "0"
        for(let c of s.split("")){
            isNum = c.match(/\d/g) != null;
            if(isNum && !numDone){
                grade += c;
            }else{
                numDone = true;
                addons.push(c);
            }
        }

        return {
            grade: parseInt(grade),
            addons: addons
        }
    }

    resolveData(): Promise<{data: ReplacementData, doNotify: boolean}>{
        return new Promise((resolve, reject) => {
            
            var options = {
                host: "schule-infoportal.de",
                path: "/infoscreen/?type=student&days=5&amp;theme=classic&amp;news=1&amp;absent=&amp;absent2=1",
                headers: {
                    'Content-Type': "text/xml;charset=UTF-8",
                    "Authorization": "Basic " + Buffer.from(config.dataResolver.username + ":" + config.dataResolver.password).toString("base64")
                },
                encoding: null
            }

            this.get(options).then(data => {
                const { document } = (new JSDOM(data)).window

                var dailyTables = document.querySelectorAll(".daily_table")
                var days = new Array<ReplacementDay>();
                
                for(let dailyTable of dailyTables){

                    var skip = 3
                    var replacements = [];
                    var count = 0;


                    for(let e of dailyTable.querySelectorAll("tr")){
                        if(skip > 0) { skip--; continue; }
                        // console.log(e);
                        var d = e.querySelectorAll("td");
                        try{
                            if(d.length <= 1){
                                continue;
                            }

                            replacements.push({
                                grade: this.parseGrade(d[0].textContent.trim() != "" ? d[0].textContent : replacements[replacements.length - 1].grade),
                                hour: d[1].textContent,
                                away: d[2].textContent,
                                representative: d[3].textContent,
                                room: d[4].textContent,
                                info: d[5].textContent
                            })
                        }catch(e){
                            console.log(e)
                            console.log(count, d[0].textContent)
                        }
                        count++;
                    }

                    var day = dailyTable.querySelectorAll("tr")[0].textContent;
                    var replDay: ReplacementDay = {
                        date: 0,
                        replacements: replacements,
                        news: []
                    }

                    days.push(replDay);

                }
                
                
                
                var allNews: {day: number, news: string[]}[] = [];
                var newsEls = document.querySelectorAll(".news");
                

                for(var n = 0; n < newsEls.length; n++){
                    var newsEl = newsEls[n];
                    
                    var news = [];

                    // news.push()
                
                    var str = newsEl.querySelector("span").innerHTML.split("<br>");
                    for(var j = 0; j < str.length; j++){
                        if(str[j] == "") continue;
                        news.push(str[j]);
                    }

                    allNews.push({
                        day: +moment(newsEl.querySelector("p").textContent, "DD.MM.YYYY"),
                        news: news
                    })
                
                }

                allNews.sort((a, b) => a.day - b.day)

                for(let i = 0; i < allNews.length; i++){
                    days[i].date = allNews[i].day;
                    days[i].news = allNews[i].news;
                }
                
                var lastUpdate = document.querySelectorAll("div")[document.querySelectorAll("div").length - 1].textContent.replace("\n", "").substring(56, 56 + 19).replace("\u00a0", " ");


                var result = {
                    days: days,
                    lastUpdate: +moment(lastUpdate, "DD.MM.YYYY HH:mm:ss"),
                    lastCrawl: new Date().getTime()
                }
                
                var doNotify = this.data != null && this.data.lastUpdate < result.lastUpdate
                if(doNotify){
                    var logIfError = (err) => { if( err ) console.error(err) }
                    fs.writeFile(__dirname + "/../tmp/previousData.json", JSON.stringify(this.data), logIfError)
                    fs.writeFile(__dirname + "/../tmp/latestData.json", JSON.stringify(result), logIfError)
                }

                this.data = result;
            
                resolve({ data: this.data, doNotify: doNotify})
 
            }).catch(err => {
                reject(err);
            })



        })
    }


    private get(options: https.RequestOptions): Promise<string>{
        return new Promise<string>((resolve, reject) => {
            var req = https.get(options, res => {
                var chunks = []
                res.on("data", data => {
                    chunks.push(data)
                }).on("error", err => {
                    reject(err)
                }).on("end", () => {
                    resolve(iconv.decode(Buffer.concat(chunks), "UTF-8"))
                })
            })
            req.on("error", err => {
                reject(err);
            })
        })
    }

}

export interface ReplacementData{
    days: ReplacementDay[],
    lastUpdate: number,
    lastCrawl: number
}

export interface ReplacementDay{
    date: number
    news: string[],
    replacements: Replacement[]
}

export interface Replacement{
    grade?: Grade,
    hour?: string,
    away?: string,
    representative?: string,
    room?: string,
    info?: string,
    addable?: string,
}

export interface Grade{
    grade: number,
    addons: string[]
}