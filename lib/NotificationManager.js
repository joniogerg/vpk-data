"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const https = require("https");
const config = require("./../config");
class NotificationManager {
    checkForUpdates() {
    }
    sendNotification() {
        var key = config.notificationManager.key;
        var req = https.request({
            host: "fcm.googleapis.com",
            path: "/fcm/send",
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "key=" + key
            }
        }, res => {
            res.setEncoding("utf8");
            res.on("data", chunk => {
                console.log(chunk);
            });
        });
        req.write(JSON.stringify({
            data: {},
            condition: "!('t' in topics)"
        }));
        req.end();
    }
}
exports.NotificationManager = NotificationManager;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTm90aWZpY2F0aW9uTWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk5vdGlmaWNhdGlvbk1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDOUIsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFBO0FBRXJDLE1BQWEsbUJBQW1CO0lBSzVCLGVBQWU7SUFFZixDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLEdBQUcsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQ3BCLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsSUFBSSxFQUFFLFdBQVc7WUFDakIsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ0wsY0FBYyxFQUFFLGtCQUFrQjtnQkFDbEMsZUFBZSxFQUFFLE1BQU0sR0FBRyxHQUFHO2FBQ2hDO1NBQ0osRUFBRSxHQUFHLENBQUMsRUFBRTtZQUNMLEdBQUcsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7WUFDdkIsR0FBRyxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDdEIsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQTtRQUdGLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNyQixJQUFJLEVBQUUsRUFFTDtZQUNELFNBQVMsRUFBRSxrQkFBa0I7U0FDaEMsQ0FBQyxDQUFDLENBQUE7UUFDSCxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDZCxDQUFDO0NBQ0o7QUFuQ0Qsa0RBbUNDIn0=