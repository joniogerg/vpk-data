"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const Auth_1 = require("./Auth");
class RESTView {
    constructor(auth, data) {
        this.auth = auth;
        this.data = data;
        this.app = express();
        this.app.use((req, res, next) => {
            req["authenticator"] = this.auth;
            next();
        });
        this.app.get("/", this.authenticate, (req, res) => {
            // auth.authenticate(AuthMethod.STATIC, "karo", "ka-gy-ro-19").then(response => {
            //     console.log(response)
            //     res.send(response)
            // })
            res.send({
                successful: true
            });
        });
        this.app.get("/auth", (req, res) => {
            if (!req.query.username || !req.query.password) {
                res.send({
                    error: "Kein Benutzername oder Password angegeben"
                });
                return;
            }
            auth.authenticate(Auth_1.AuthMethod.STATIC, req.query.username, req.query.password).then(response => {
                res.send(response);
            }).catch(() => {
                res.send({
                    error: "Benutzername oder Password sind falsch"
                });
            });
        });
        this.app.get("/filtered/:course", this.authenticate, (req, res) => {
            var filtered = this.data.getReplacementsFiltered(req.params["course"], req.query.teachers || "");
            filtered.days = filtered.days.slice(0, parseInt(req.query.days != undefined ? req.query.days : 2));
            res.send(filtered);
        });
        this.app.get("/unfiltered", this.authenticate, (req, res) => {
            res.send(this.data.getReplacements());
        });
        this.app.listen(8085);
    }
    authenticate(req, res, next) {
        var rejectRequest = function () {
            res.status(401);
            res.send({
                error: "Unauthorized",
                errorCode: 401
            });
        };
        if (!req.headers["x-authentication"]) {
            rejectRequest();
            return;
        }
        else {
            req.authenticator.authenticate(Auth_1.AuthMethod.TOKEN, req.headers["x-authentication"]).then(authRes => {
                if (authRes.valid) {
                    next();
                }
                else {
                    rejectRequest();
                }
            });
        }
    }
}
exports.RESTView = RESTView;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUkVTVFZpZXcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJSRVNUVmlldy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1DQUFrQztBQUNsQyxpQ0FBMEM7QUFHMUMsTUFBYSxRQUFRO0lBSWpCLFlBQW1CLElBQVUsRUFBUyxJQUFrQjtRQUFyQyxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVMsU0FBSSxHQUFKLElBQUksQ0FBYztRQUZoRCxRQUFHLEdBQUcsT0FBTyxFQUFFLENBQUM7UUFJcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFO1lBQzVCLEdBQUcsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2pDLElBQUksRUFBRSxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUM5QyxpRkFBaUY7WUFDakYsNEJBQTRCO1lBQzVCLHlCQUF5QjtZQUN6QixLQUFLO1lBRUwsR0FBRyxDQUFDLElBQUksQ0FBQztnQkFDTCxVQUFVLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUE7UUFFTixDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUMvQixJQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBQztnQkFDMUMsR0FBRyxDQUFDLElBQUksQ0FBQztvQkFDTCxLQUFLLEVBQUUsMkNBQTJDO2lCQUNyRCxDQUFDLENBQUE7Z0JBQ0YsT0FBTzthQUNWO1lBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxpQkFBVSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDekYsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUM7b0JBQ0wsS0FBSyxFQUFFLHdDQUF3QztpQkFDbEQsQ0FBQyxDQUFBO1lBQ04sQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQTtRQUdGLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDOUQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBRWpHLFFBQVEsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ2xHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDdEIsQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUN4RCxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQTtRQUN6QyxDQUFDLENBQUMsQ0FBQTtRQUdGLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBRXpCLENBQUM7SUFHRCxZQUFZLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJO1FBQ3ZCLElBQUksYUFBYSxHQUFHO1lBQ2hCLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDZixHQUFHLENBQUMsSUFBSSxDQUFDO2dCQUNMLEtBQUssRUFBRSxjQUFjO2dCQUNyQixTQUFTLEVBQUUsR0FBRzthQUNqQixDQUFDLENBQUE7UUFDTixDQUFDLENBQUE7UUFDRCxJQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFDO1lBQ2hDLGFBQWEsRUFBRSxDQUFBO1lBQ2YsT0FBTztTQUNWO2FBQUk7WUFDRCxHQUFHLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxpQkFBVSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3ZHLElBQUcsT0FBTyxDQUFDLEtBQUssRUFBQztvQkFDYixJQUFJLEVBQUUsQ0FBQztpQkFDVjtxQkFBSTtvQkFDRCxhQUFhLEVBQUUsQ0FBQztpQkFDbkI7WUFDTCxDQUFDLENBQUMsQ0FBQTtTQUNMO0lBQ0wsQ0FBQztDQUdKO0FBaEZELDRCQWdGQyJ9