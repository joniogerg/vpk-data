import { DataResolver } from "./DataResolver";
import { NotificationManager } from "./NotificationManager";

export class TaskManager{

    DATA_REFRESH = 1000*20;

    private intervalRunning = null;

    constructor(private dataResolver: DataResolver, private notify: NotificationManager){

    }


    startInterval(){
        this.intervalRunning = true;
        this.resolveData();
    }

    stopInterval(){
        this.intervalRunning = false;
    }

    resolveData(){
        var self = this;
        function doAgainLater(){
            setTimeout( () => {
                if(self.intervalRunning){
                    self.resolveData();
                }
            }, self.DATA_REFRESH)
        }
        this.dataResolver.resolveData().then( data => {

            if(data.doNotify){

            }


            doAgainLater();
        }).catch(err => {
            console.error(err);
            doAgainLater();
        })
    }



}