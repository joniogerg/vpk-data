"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const https = require("https");
const iconv = require("iconv-lite");
const jsdom = require("jsdom");
const moment = require("moment");
const fs = require("fs");
const { JSDOM } = jsdom;
const config = require("./../config");
class DataResolver {
    constructor() {
        this.data = { lastCrawl: 1022130, lastUpdate: 1023123, days: [] };
    }
    getReplacements() {
        return this.data;
    }
    getReplacementsFiltered(course, teachersS = "") {
        if (this.data == null) {
            return null;
        }
        var data = {
            days: [],
            lastUpdate: this.data.lastUpdate,
            lastCrawl: this.data.lastCrawl
        };
        for (let d of this.data.days) {
            data.days.push({
                date: d.date,
                replacements: d.replacements,
                news: d.news
            });
        }
        var grade = this.parseGrade(course);
        var teachers = teachersS.split(" ");
        var isInSeniorLevel = grade.grade >= 11;
        for (let i = 0; i < data.days.length; i++) {
            var day = data.days[i];
            var replacements = [];
            for (let repl of day.replacements) {
                if (isInSeniorLevel && (repl.grade.grade == grade.grade && repl.grade.addons.includes("Q"))) {
                    if (teachers.includes(repl.away)) {
                        replacements.push(repl);
                        continue;
                    }
                }
                if (repl.grade.grade == grade.grade) {
                    if (grade.addons.length == 0) {
                        replacements.push(repl);
                    }
                    else {
                        for (let a of grade.addons) {
                            if (repl.grade.addons.includes(a)) {
                                replacements.push(repl);
                                continue;
                            }
                        }
                    }
                }
            }
            data.days[i].replacements = replacements;
        }
        return data;
    }
    parseGrade(s) {
        if (typeof s === "object") {
            return s;
        }
        if (s == "Q11" || s == "Q12") {
            return {
                grade: parseInt(s.replace("Q", "")),
                addons: ["Q"]
            };
        }
        var isNum = false;
        var numDone = false;
        var addons = [];
        var grade = "0";
        for (let c of s.split("")) {
            isNum = c.match(/\d/g) != null;
            if (isNum && !numDone) {
                grade += c;
            }
            else {
                numDone = true;
                addons.push(c);
            }
        }
        return {
            grade: parseInt(grade),
            addons: addons
        };
    }
    resolveData() {
        return new Promise((resolve, reject) => {
            var options = {
                host: "schule-infoportal.de",
                path: "/infoscreen/?type=student&days=5&amp;theme=classic&amp;news=1&amp;absent=&amp;absent2=1",
                headers: {
                    'Content-Type': "text/xml;charset=UTF-8",
                    "Authorization": "Basic " + Buffer.from(config.dataResolver.username + ":" + config.dataResolver.password).toString("base64")
                },
                encoding: null
            };
            this.get(options).then(data => {
                const { document } = (new JSDOM(data)).window;
                var dailyTables = document.querySelectorAll(".daily_table");
                var days = new Array();
                for (let dailyTable of dailyTables) {
                    var skip = 3;
                    var replacements = [];
                    var count = 0;
                    for (let e of dailyTable.querySelectorAll("tr")) {
                        if (skip > 0) {
                            skip--;
                            continue;
                        }
                        // console.log(e);
                        var d = e.querySelectorAll("td");
                        try {
                            if (d.length <= 1) {
                                continue;
                            }
                            replacements.push({
                                grade: this.parseGrade(d[0].textContent.trim() != "" ? d[0].textContent : replacements[replacements.length - 1].grade),
                                hour: d[1].textContent,
                                away: d[2].textContent,
                                representative: d[3].textContent,
                                room: d[4].textContent,
                                info: d[5].textContent
                            });
                        }
                        catch (e) {
                            console.log(e);
                            console.log(count, d[0].textContent);
                        }
                        count++;
                    }
                    var day = dailyTable.querySelectorAll("tr")[0].textContent;
                    var replDay = {
                        date: 0,
                        replacements: replacements,
                        news: []
                    };
                    days.push(replDay);
                }
                var allNews = [];
                var newsEls = document.querySelectorAll(".news");
                for (var n = 0; n < newsEls.length; n++) {
                    var newsEl = newsEls[n];
                    var news = [];
                    // news.push()
                    var str = newsEl.querySelector("span").innerHTML.split("<br>");
                    for (var j = 0; j < str.length; j++) {
                        if (str[j] == "")
                            continue;
                        news.push(str[j]);
                    }
                    allNews.push({
                        day: +moment(newsEl.querySelector("p").textContent, "DD.MM.YYYY"),
                        news: news
                    });
                }
                allNews.sort((a, b) => a.day - b.day);
                for (let i = 0; i < allNews.length; i++) {
                    days[i].date = allNews[i].day;
                    days[i].news = allNews[i].news;
                }
                var lastUpdate = document.querySelectorAll("div")[document.querySelectorAll("div").length - 1].textContent.replace("\n", "").substring(56, 56 + 19).replace("\u00a0", " ");
                var result = {
                    days: days,
                    lastUpdate: +moment(lastUpdate, "DD.MM.YYYY HH:mm:ss"),
                    lastCrawl: new Date().getTime()
                };
                var doNotify = this.data != null && this.data.lastUpdate < result.lastUpdate;
                if (doNotify) {
                    var logIfError = (err) => { if (err)
                        console.error(err); };
                    fs.writeFile(__dirname + "/../tmp/previousData.json", JSON.stringify(this.data), logIfError);
                    fs.writeFile(__dirname + "/../tmp/latestData.json", JSON.stringify(result), logIfError);
                }
                this.data = result;
                resolve({ data: this.data, doNotify: doNotify });
            }).catch(err => {
                reject(err);
            });
        });
    }
    get(options) {
        return new Promise((resolve, reject) => {
            var req = https.get(options, res => {
                var chunks = [];
                res.on("data", data => {
                    chunks.push(data);
                }).on("error", err => {
                    reject(err);
                }).on("end", () => {
                    resolve(iconv.decode(Buffer.concat(chunks), "UTF-8"));
                });
            });
            req.on("error", err => {
                reject(err);
            });
        });
    }
}
exports.DataResolver = DataResolver;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0YVJlc29sdmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiRGF0YVJlc29sdmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsK0JBQThCO0FBQzlCLG9DQUFtQztBQUNuQywrQkFBOEI7QUFDOUIsaUNBQWdDO0FBQ2hDLHlCQUF5QjtBQUN6QixNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsS0FBSyxDQUFDO0FBQ3hCLHNDQUFzQztBQUV0QyxNQUFhLFlBQVk7SUFBekI7UUFFWSxTQUFJLEdBQW9CLEVBQUcsU0FBUyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUMsQ0FBQztJQXdPMUYsQ0FBQztJQXJPRyxlQUFlO1FBQ1gsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFBO0lBQ3BCLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxNQUFjLEVBQUUsWUFBb0IsRUFBRTtRQUMxRCxJQUFHLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFDO1lBQ2pCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFFRCxJQUFJLElBQUksR0FBb0I7WUFDeEIsSUFBSSxFQUFFLEVBQUU7WUFDUixVQUFVLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVO1lBQ2hDLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVM7U0FDakMsQ0FBQztRQUVGLEtBQUksSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUM7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ1gsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO2dCQUNaLFlBQVksRUFBRSxDQUFDLENBQUMsWUFBWTtnQkFDNUIsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO2FBQ2YsQ0FBQyxDQUFBO1NBQ0w7UUFDRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ25DLElBQUksUUFBUSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFcEMsSUFBSSxlQUFlLEdBQUcsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFFeEMsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO1lBQ3JDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLEtBQUksSUFBSSxJQUFJLElBQUksR0FBRyxDQUFDLFlBQVksRUFBQztnQkFDN0IsSUFBRyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFDO29CQUN2RixJQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDO3dCQUM1QixZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN4QixTQUFTO3FCQUNaO2lCQUNKO2dCQUNELElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssRUFBQztvQkFDL0IsSUFBRyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUM7d0JBQ3hCLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQzNCO3lCQUFJO3dCQUNELEtBQUksSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBQzs0QkFDdEIsSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUM7Z0NBQzdCLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ3hCLFNBQVM7NkJBQ1o7eUJBQ0o7cUJBQ0o7aUJBQ0o7YUFDSjtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztTQUM1QztRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxVQUFVLENBQUMsQ0FBUztRQUN4QixJQUFHLE9BQU8sQ0FBQyxLQUFLLFFBQVEsRUFBQztZQUNyQixPQUFPLENBQUMsQ0FBQztTQUNaO1FBQ0QsSUFBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxLQUFLLEVBQUM7WUFDeEIsT0FBTztnQkFDSCxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUM7YUFDaEIsQ0FBQTtTQUNKO1FBRUQsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFDZixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUE7UUFDZixLQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUM7WUFDckIsS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDO1lBQy9CLElBQUcsS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFDO2dCQUNqQixLQUFLLElBQUksQ0FBQyxDQUFDO2FBQ2Q7aUJBQUk7Z0JBQ0QsT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2xCO1NBQ0o7UUFFRCxPQUFPO1lBQ0gsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUM7WUFDdEIsTUFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQTtJQUNMLENBQUM7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUVuQyxJQUFJLE9BQU8sR0FBRztnQkFDVixJQUFJLEVBQUUsc0JBQXNCO2dCQUM1QixJQUFJLEVBQUUseUZBQXlGO2dCQUMvRixPQUFPLEVBQUU7b0JBQ0wsY0FBYyxFQUFFLHdCQUF3QjtvQkFDeEMsZUFBZSxFQUFFLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7aUJBQ2hJO2dCQUNELFFBQVEsRUFBRSxJQUFJO2FBQ2pCLENBQUE7WUFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDMUIsTUFBTSxFQUFFLFFBQVEsRUFBRSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUE7Z0JBRTdDLElBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQTtnQkFDM0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxLQUFLLEVBQWtCLENBQUM7Z0JBRXZDLEtBQUksSUFBSSxVQUFVLElBQUksV0FBVyxFQUFDO29CQUU5QixJQUFJLElBQUksR0FBRyxDQUFDLENBQUE7b0JBQ1osSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO29CQUN0QixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7b0JBR2QsS0FBSSxJQUFJLENBQUMsSUFBSSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUM7d0JBQzNDLElBQUcsSUFBSSxHQUFHLENBQUMsRUFBRTs0QkFBRSxJQUFJLEVBQUUsQ0FBQzs0QkFBQyxTQUFTO3lCQUFFO3dCQUNsQyxrQkFBa0I7d0JBQ2xCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDakMsSUFBRzs0QkFDQyxJQUFHLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFDO2dDQUNiLFNBQVM7NkJBQ1o7NEJBRUQsWUFBWSxDQUFDLElBQUksQ0FBQztnQ0FDZCxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dDQUN0SCxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVc7Z0NBQ3RCLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVztnQ0FDdEIsY0FBYyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXO2dDQUNoQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVc7Z0NBQ3RCLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVzs2QkFDekIsQ0FBQyxDQUFBO3lCQUNMO3dCQUFBLE9BQU0sQ0FBQyxFQUFDOzRCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7NEJBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFBO3lCQUN2Qzt3QkFDRCxLQUFLLEVBQUUsQ0FBQztxQkFDWDtvQkFFRCxJQUFJLEdBQUcsR0FBRyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO29CQUMzRCxJQUFJLE9BQU8sR0FBbUI7d0JBQzFCLElBQUksRUFBRSxDQUFDO3dCQUNQLFlBQVksRUFBRSxZQUFZO3dCQUMxQixJQUFJLEVBQUUsRUFBRTtxQkFDWCxDQUFBO29CQUVELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBRXRCO2dCQUlELElBQUksT0FBTyxHQUFvQyxFQUFFLENBQUM7Z0JBQ2xELElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFHakQsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0JBQ25DLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFeEIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO29CQUVkLGNBQWM7b0JBRWQsSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUMvRCxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQzt3QkFDL0IsSUFBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTs0QkFBRSxTQUFTO3dCQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNyQjtvQkFFRCxPQUFPLENBQUMsSUFBSSxDQUFDO3dCQUNULEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUM7d0JBQ2pFLElBQUksRUFBRSxJQUFJO3FCQUNiLENBQUMsQ0FBQTtpQkFFTDtnQkFFRCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7Z0JBRXJDLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUNuQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7b0JBQzlCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDbEM7Z0JBRUQsSUFBSSxVQUFVLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFHM0ssSUFBSSxNQUFNLEdBQUc7b0JBQ1QsSUFBSSxFQUFFLElBQUk7b0JBQ1YsVUFBVSxFQUFFLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxxQkFBcUIsQ0FBQztvQkFDdEQsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFO2lCQUNsQyxDQUFBO2dCQUVELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUE7Z0JBQzVFLElBQUcsUUFBUSxFQUFDO29CQUNSLElBQUksVUFBVSxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsR0FBRyxJQUFJLEdBQUc7d0JBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQTtvQkFDMUQsRUFBRSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUE7b0JBQzVGLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLHlCQUF5QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUE7aUJBQzFGO2dCQUVELElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO2dCQUVuQixPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FBQTtZQUVuRCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ1gsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxDQUFBO1FBSU4sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBR08sR0FBRyxDQUFDLE9BQTZCO1FBQ3JDLE9BQU8sSUFBSSxPQUFPLENBQVMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLEVBQUU7Z0JBQy9CLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQTtnQkFDZixHQUFHLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtvQkFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDckIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsRUFBRTtvQkFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFBO2dCQUNmLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFO29CQUNkLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQTtnQkFDekQsQ0FBQyxDQUFDLENBQUE7WUFDTixDQUFDLENBQUMsQ0FBQTtZQUNGLEdBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQixNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7Q0FFSjtBQTFPRCxvQ0EwT0MifQ==