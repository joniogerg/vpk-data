"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = require("./../config");
const jwt = require("jsonwebtoken");
const sqlite3_1 = require("sqlite3");
const index_1 = require("./../index");
class Auth {
    constructor() {
        this.SECRET = config.auth.secret;
        this.DEFAULT_LIFETIME = 1000 * 60 * 60 * 24 * 30 * 11; // Lifetime 11 months
        this.LAST_VALIDATION_THREADHOLD = 1000 * 60 * 60 * 24 * 7 * 2; // Threadhold 2 week
        sqlite3_1.verbose();
        console.log("Opening database at " + index_1.DIR + "/db/auth.db");
        this.db = new sqlite3_1.Database(index_1.DIR + "/db/auth.db", sqlite3_1.OPEN_READWRITE | sqlite3_1.OPEN_CREATE, (err) => {
            if (err) {
                console.error(err.message);
            }
            else {
                console.log("Connected to database ", process.cwd());
                this.db.run('CREATE TABLE IF NOT EXISTS tokens(token TEXT, lastValidation FLOAT)');
                this.cleanDatabase();
            }
        });
    }
    authenticate(method, username, password) {
        return new Promise((resolve, reject) => {
            if (method == AuthMethod.STATIC) {
                if (username == config.auth.static.username && password == config.auth.static.password) {
                    this.createToken(this.DEFAULT_LIFETIME).then(token => {
                        resolve({
                            method: method,
                            token: token
                        });
                    }).catch(err => {
                        console.error(err);
                    });
                }
                else {
                    reject(false);
                }
            }
            else if (method == AuthMethod.TOKEN) {
                this.validateToken(username).then(valid => {
                    resolve({
                        method: method,
                        valid: valid
                    });
                });
            }
        });
    }
    validateToken(token) {
        return new Promise((resolve, reject) => {
            this.db.get("SELECT * FROM tokens WHERE token=?", [token], (err, row) => {
                if (err == null && row != null && row.lastValidation + this.LAST_VALIDATION_THREADHOLD > new Date().getTime()) {
                    this.db.run("UPDATE 'tokens' SET lastValidation=? WHERE token=?", [new Date().getTime(), token], (err) => {
                        resolve(true);
                    });
                }
                else {
                    this.db.run("DELETE FROM tokens WHERE token=?", [token], this.silenterr);
                    resolve(false);
                }
            });
        });
    }
    createToken(lifetime) {
        return new Promise((resolve, reject) => {
            var token = jwt.sign({
                expiresAfter: new Date().getTime() + lifetime
            }, this.SECRET);
            this.db.run("INSERT INTO tokens VALUES(?, ?)", [token, new Date().getTime()], (err) => {
                console.log(err);
                if (err) {
                    reject(err);
                }
                else {
                    resolve(token);
                }
            });
        });
    }
    cleanDatabase() {
        this.db.run("DELETE FROM tokens WHERE lastValidation < ?", [new Date().getTime() - this.LAST_VALIDATION_THREADHOLD], this.silenterr);
    }
    silenterr(err) {
        if (err)
            console.error(err);
    }
}
exports.Auth = Auth;
var AuthMethod;
(function (AuthMethod) {
    AuthMethod[AuthMethod["STATIC"] = 0] = "STATIC";
    AuthMethod[AuthMethod["TOKEN"] = 1] = "TOKEN";
})(AuthMethod = exports.AuthMethod || (exports.AuthMethod = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkF1dGgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBc0M7QUFDdEMsb0NBQW1DO0FBQ25DLHFDQUEyRjtBQUMzRixzQ0FBZ0M7QUFFaEMsTUFBYSxJQUFJO0lBUWI7UUFOUSxXQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDNUIscUJBQWdCLEdBQWdCLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFBLENBQUMscUJBQXFCO1FBQ25GLCtCQUEwQixHQUFNLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBLENBQUUsb0JBQW9CO1FBS3JGLGlCQUFlLEVBQUUsQ0FBQztRQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLFdBQUcsR0FBRyxhQUFhLENBQUMsQ0FBQTtRQUN6RCxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksa0JBQVEsQ0FBQyxXQUFHLEdBQUcsYUFBYSxFQUFFLHdCQUFjLEdBQUcscUJBQVcsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQzlFLElBQUcsR0FBRyxFQUFDO2dCQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO2FBQzdCO2lCQUFJO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUE7Z0JBQ3BELElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLHFFQUFxRSxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUN4QjtRQUNMLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLFlBQVksQ0FBQyxNQUFrQixFQUFFLFFBQWdCLEVBQUUsUUFBaUI7UUFDdkUsT0FBTyxJQUFJLE9BQU8sQ0FBZSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNqRCxJQUFHLE1BQU0sSUFBSSxVQUFVLENBQUMsTUFBTSxFQUFDO2dCQUMzQixJQUFHLFFBQVEsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksUUFBUSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBQztvQkFDbEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxJQUFJLENBQUUsS0FBSyxDQUFDLEVBQUU7d0JBQ2xELE9BQU8sQ0FBQzs0QkFDSixNQUFNLEVBQUUsTUFBTTs0QkFDZCxLQUFLLEVBQUUsS0FBSzt5QkFDZixDQUFDLENBQUE7b0JBQ04sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUNYLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUE7b0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFJO29CQUNELE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtpQkFDaEI7YUFDSjtpQkFBSyxJQUFHLE1BQU0sSUFBSSxVQUFVLENBQUMsS0FBSyxFQUFDO2dCQUNoQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDdEMsT0FBTyxDQUFDO3dCQUNKLE1BQU0sRUFBRSxNQUFNO3dCQUNkLEtBQUssRUFBRSxLQUFLO3FCQUNmLENBQUMsQ0FBQTtnQkFDTixDQUFDLENBQUMsQ0FBQTthQUNMO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sYUFBYSxDQUFDLEtBQUs7UUFDdkIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxvQ0FBb0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUNwRSxJQUFHLEdBQUcsSUFBSSxJQUFJLElBQUksR0FBRyxJQUFJLElBQUksSUFBSSxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFDO29CQUN6RyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxvREFBb0QsRUFBRSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRTt3QkFDckcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO29CQUNqQixDQUFDLENBQUMsQ0FBQTtpQkFDTDtxQkFBSTtvQkFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtvQkFDeEUsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO2lCQUNqQjtZQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sV0FBVyxDQUFDLFFBQWdCO1FBQ2hDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkMsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztnQkFDakIsWUFBWSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsUUFBUTthQUNoRCxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUNmLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxFQUFFLENBQUMsS0FBSyxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNsRixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNqQixJQUFHLEdBQUcsRUFBQztvQkFDSCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ2Y7cUJBQUk7b0JBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNsQjtZQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sYUFBYTtRQUNqQixJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyw2Q0FBNkMsRUFBRSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO0lBQ3hJLENBQUM7SUFFTyxTQUFTLENBQUMsR0FBRztRQUNqQixJQUFHLEdBQUc7WUFDRixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLENBQUM7Q0FFSjtBQXhGRCxvQkF3RkM7QUFFRCxJQUFZLFVBR1g7QUFIRCxXQUFZLFVBQVU7SUFDbEIsK0NBQVUsQ0FBQTtJQUNWLDZDQUFTLENBQUE7QUFDYixDQUFDLEVBSFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFHckIifQ==