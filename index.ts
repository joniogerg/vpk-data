import { Main } from "./lib/main";
import * as child_proc from "child_process"

export const SERVER_VERSION = 3;

console.log("Running server version " + SERVER_VERSION)
console.log("Last commit: " + child_proc.execSync('git show --summary --format="%ar: \\"%s\\""').toString("utf8").replace("\n", ""))

export const DIR = __dirname + ""
const main = new Main();